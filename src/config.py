import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())


class Hotel:
    def __init__(self, desc, num) -> None:
        self.desc = desc
        self.total_num = num
        self.current_num = num

    def get_desc(self):
        return self.desc

    def __str__(self) -> str:
        return f"{self.total_num} {self.current_num}"


class Config(object):
    HOST = os.environ.get("DB_HOST")
    NAME = os.environ.get("DB_NAME")
    USERNAME = os.environ.get("DB_USER")
    PASSWORD = os.environ.get("DB_PASSWORD")

    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

    DATA_TABLE_NAME = "infocity_booking_bookings"

    PATH_TO_LOGS = "/web/src/logs/"

    ENV = "prod"

    ID_TO_HOTEL = {
        1: Hotel("Джуниор Сюит", 1),
        2: Hotel("Стандартный двухместный номер", 8),
        3: Hotel("Трёхместный семейный номер", 2),
        4: Hotel("Домик с тремя спальнями", 1),
        5: Hotel("Семейный четырёхместный номер", 2),
        6: Hotel("Домик с двумя спальнями", 2),
    }

    class FlaskApp(object):
        PORT = 5087
        HOST = "0.0.0.0"
        DEBUG = True

        BASE_URL = f"http://{HOST}:{PORT}"


app_config = Config()
