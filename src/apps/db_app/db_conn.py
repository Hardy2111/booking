import mysql.connector
import pandas as pd
from abc import ABC, abstractmethod
from mysql.connector import Error

from config import app_config


class BaseSQLConnector(ABC):
    @abstractmethod
    def get_data_from_table(self, table_name):
        ...


class MySQLConnector(BaseSQLConnector):
    def __init__(self, host, user, password, database):
        self.host = host
        self.user = user
        self.password = password
        self.database = database

        try:
            self.create_connection()
        except Error as err:
            print(f"Ошибка подключения: {err}")

    def create_connection(self):
            self.connection = mysql.connector.connect(
                host=self.host, user=self.user, password=self.password, database=self.database
            )
            if self.connection.is_connected():
                print("Подключение успешно установлено.")


    def get_data_from_table(self, table_name) -> pd.DataFrame:
        try:
            with self.connection.cursor(dictionary=True) as cursor:
                query = f"SELECT * FROM {table_name};"
                return pd.read_sql(query, self.connection)
        except Error as err:
            print(f"Ошибка выполнения запроса: {err}")
            
            try:
                self.create_connection()
            except Error as err:
                print(f"Ошибка подключения: {err}")
                return pd.DataFrame()
            return self.get_data_from_table(table_name)

    def close_connection(self):
        if self.connection.is_connected():
            self.connection.close()
            print("Подключение закрыто.")

    def __del__(self):
        self.close_connection()

class MyTestSQLConnector(BaseSQLConnector):
    def get_data_from_table(self, table_name):
        return pd.read_json(f"./Data/{table_name}.json", orient="records")


my_sql_connector = (
    MySQLConnector(
        host=app_config.HOST,
        user=app_config.USERNAME,
        password=app_config.PASSWORD,
        database=app_config.NAME,
    )
    if app_config.ENV == "prod"
    else MyTestSQLConnector()
)
