import copy
from flask_wtf import FlaskForm
from datetime import datetime
from wtforms import DateField
from flask import Blueprint, render_template
import pandas as pd

from apps.db_app.db_conn import my_sql_connector
from config import app_config


bp = Blueprint("calendar", __name__, url_prefix="/booking")


def convert_data(data: pd.DataFrame):
    data["from_date"] = pd.to_datetime(data["from_date"])
    data["to_date"] = pd.to_datetime(data["to_date"])

    data['from_date'] = data['from_date'] - pd.Timedelta(days=1)
    data['to_date'] = data['to_date'] - pd.Timedelta(days=1)


def get_info_by_day(day: datetime, data: pd.DataFrame):
    return data[(data["from_date"] <= day) & (data["to_date"] > day)]


def processing_data_from_day(data: pd.DataFrame):
    id_to_hotel = copy.deepcopy(app_config.ID_TO_HOTEL)
    grouped_data = data.groupby("hotel_id").size().reset_index(name="count")

    for row in grouped_data.itertuples():
        id_to_hotel[row.hotel_id].current_num -= row.count

    id_to_hotel = {i: str(id_to_hotel[i]) for i in id_to_hotel}
    return id_to_hotel


@bp.get("/calendar")
def route_booking():
    data = my_sql_connector.get_data_from_table(app_config.DATA_TABLE_NAME)
    print(data)
    convert_data(data)
    data_by_day = get_info_by_day(
        datetime.strptime("2023-10-22 00:00:00", app_config.DATETIME_FORMAT), data
    )
    processed_data = processing_data_from_day(data_by_day)
    print(processed_data)
    return ""


class DateRangeForm(FlaskForm):
    start_date = DateField("Start Date")
    end_date = DateField("End Date")


@bp.route("/", methods=["GET", "POST"])
def index():
    form = DateRangeForm()
    data_table = None
    descriptions = {i: app_config.ID_TO_HOTEL[i].get_desc() for i in app_config.ID_TO_HOTEL}

    if form.validate_on_submit():
        data = my_sql_connector.get_data_from_table(app_config.DATA_TABLE_NAME)
        convert_data(data)

        start_date = form.start_date.data
        end_date = form.end_date.data

        date_range = pd.date_range(start_date, end_date, freq="D")
        data = {
            "Date": date_range,
            "Data": [
                processing_data_from_day(get_info_by_day(day, data))
                for day in date_range
            ],
        }
        data_table = pd.DataFrame(data)

    return render_template("index.html", form=form, data_table=data_table, descriptions=descriptions)
