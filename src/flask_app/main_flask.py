from flask import Flask
import os
import secrets


def create_app() -> Flask:
    """
    Create and configure the Flask application.

    Returns:
        Flask: The configured Flask application.
    """

    work_dir = os.getcwd()
    app = Flask(
        __name__,
        template_folder=work_dir + "/templates",
        static_folder=work_dir + "/static",
    )
    app.config["SECRET_KEY"] = secrets.token_hex(16)

    with app.app_context():
        from config import app_config

        app.config.from_object(app_config)

        from apps.routes.table_route import bp

        app.register_blueprint(bp)

        return app
