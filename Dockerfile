FROM python:3.10

WORKDIR /web
EXPOSE 5089

COPY ./pyproject.toml /web/pyproject.toml
COPY ./.env /web/.env

RUN pip install poetry
RUN poetry install --no-root --verbose
