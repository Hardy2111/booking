document.addEventListener('DOMContentLoaded', function () {
    const calendarContainer = document.getElementById('calendar');

    function createCalendar(year, month) {
        const firstDay = new Date(year, month, 1);
        const lastDay = new Date(year, month + 1, 0);
        const daysInMonth = lastDay.getDate();
        const startingDay = firstDay.getDay();

        const weekdays = ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'];

        let calendarHTML = `<div class="header">${year} - ${month + 1}</div>`;
        calendarHTML += '<div class="weekdays">';
        weekdays.forEach(day => {
            calendarHTML += `<div>${day}</div>`;
        });
        calendarHTML += '</div>';

        let day = 1;

        for (let i = 0; i < 6; i++) {
            for (let j = 0; j < 7; j++) {
                if (i === 0 && j < startingDay) {
                    calendarHTML += '<div class="day"></div>';
                } else if (day > daysInMonth) {
                    break;
                } else {
                    calendarHTML += `<div class="day">${day}</div>`;
                    day++;
                }
            }
        }

        calendarContainer.innerHTML = calendarHTML;
    }

    const today = new Date();
    createCalendar(today.getFullYear(), today.getMonth());

});
